package de.awacademy.blog.Cookiemonster.home;

import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitragDTO;
import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitragRepository;
import de.awacademy.blog.Cookiemonster.beitrag.BeitragRepository;
import de.awacademy.blog.Cookiemonster.kategorie.KategorieRepository;
import de.awacademy.blog.Cookiemonster.kommentar.KommentarDTO;
import de.awacademy.blog.Cookiemonster.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    private BeitragRepository beitragRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    KategorieRepository kategorieRepository;

    @Autowired
    BackupBeitragRepository backupBeitragRepository;


    @GetMapping("/")
    public String home(Model model ) {
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByErstellungsDatumDesc());
        model.addAttribute("kommentar", new KommentarDTO());
        model.addAttribute("benutzerverwaltung");
        model.addAttribute("kategorien", kategorieRepository.findAll());
        model.addAttribute("metaDatenelemente", new BackupBeitragDTO());
        return "home";
    }


}
