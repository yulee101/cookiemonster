package de.awacademy.blog.Cookiemonster.kommentar;

import javax.validation.constraints.NotEmpty;

public class KommentarDTO {

    @NotEmpty
    private String text = "";
    private long id=0;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
