package de.awacademy.blog.Cookiemonster.kommentar;

import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;


public interface KommentarRepository extends JpaRepository <Kommentar,Long> {
    @Modifying
    @Transactional
    void deleteKommentarById(long id);

}
