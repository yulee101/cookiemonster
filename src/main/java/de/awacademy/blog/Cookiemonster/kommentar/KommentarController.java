package de.awacademy.blog.Cookiemonster.kommentar;

import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;
import de.awacademy.blog.Cookiemonster.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class KommentarController {

    @Autowired
    KommentarRepository kommentarRepository;


    @PostMapping("/kommentar")
    public String kommentarErstellen(@ModelAttribute("kommentar") @Valid KommentarDTO kommentarDTO,
                                     BindingResult bindingResult,
                                     @ModelAttribute("currentUser") User currentUser, @RequestParam("beitragId") Beitrag beitrag) {
        if (bindingResult.hasErrors()) {
            return "redirect:/";
        }
        Kommentar kommentar = new Kommentar(kommentarDTO.getText(), currentUser, beitrag);
        kommentarRepository.save(kommentar);
        return "redirect:/";
    }

    @PostMapping("/deleteKommentar")
    public String deleteKommentar(@RequestParam("kommentarId") Kommentar kommentar) {
        kommentarRepository.deleteKommentarById(kommentar.getId());
        return "redirect:";
    }

}