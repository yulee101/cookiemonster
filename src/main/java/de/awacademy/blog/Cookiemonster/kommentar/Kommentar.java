package de.awacademy.blog.Cookiemonster.kommentar;

import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;
import de.awacademy.blog.Cookiemonster.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;
    private Instant erstellungsDatum;

    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private User user;

    public Kommentar() {
    }

    public Kommentar(String text, User user, Beitrag beitrag) {
        this.text = text;
        this.user = user;
        this.erstellungsDatum = Instant.now();
        this.beitrag = beitrag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setErstellungsDatum(Instant erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
