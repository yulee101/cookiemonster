package de.awacademy.blog.Cookiemonster.beitrag;

import de.awacademy.blog.Cookiemonster.kategorie.Kategorie;
import de.awacademy.blog.Cookiemonster.kategorie.KategorieRepository;
import de.awacademy.blog.Cookiemonster.kommentar.KommentarRepository;
import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitrag;
import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitragDTO;
import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitragRepository;
import de.awacademy.blog.Cookiemonster.session.SessionRepository;
import de.awacademy.blog.Cookiemonster.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@Controller
public class BeitragController {

    @Autowired
    BeitragRepository beitragRepository;

    @Autowired
    KommentarRepository kommentarRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    BackupBeitragRepository backupBeitragRepository;

    @Autowired
    KategorieRepository kategorieRepository;

    @GetMapping("/beitragErstellen")
    public String beitragErstellen(Model model) {
        model.addAttribute("beitrag", new BeitragDTO());
        model.addAttribute("backupBeitrag", new BackupBeitragDTO());
        model.addAttribute("kategorien", kategorieRepository.findAll());
        return "beitragErstellen";
    }

    @PostMapping("/beitragErstellen")
    public String beitragErstellen(@ModelAttribute("beitrag") BeitragDTO beitragDTO, @ModelAttribute("backupBeitrag") BackupBeitragDTO BackupBeitragDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser) {
        if (bindingResult.hasErrors()) {
            return "beitragErstellen";
        }
        Beitrag beitrag = new Beitrag(beitragDTO.getUeberschrift(), beitragDTO.getText(), currentUser, beitragDTO.getBeitragStatus());
        for (Long kategorieId : beitragDTO.getKategorieIdList()) {
            beitrag.getKategorien().add(kategorieRepository.findById(kategorieId).get());
        }
        BackupBeitrag backupBeitrag = new BackupBeitrag(beitragDTO.getUeberschrift(), beitragDTO.getText(), currentUser, beitragDTO.getBeitragStatus());

        beitragRepository.save(beitrag);
        beitrag.getBackupBeitraege().add(backupBeitrag);
        backupBeitragRepository.save(backupBeitrag);
        return "redirect:/";
    }


    @PostMapping("/beitragBearbeitenLaden")
    public String beitragBearbeiten(Model model, @RequestParam("beitragId") long id) {
        Beitrag beitrag = beitragRepository.findById(id).get();
        model.addAttribute("beitragId", beitrag.getId());
        BeitragDTO beitragDTO = new BeitragDTO(beitrag.getText(), beitrag.getUeberschrift(), beitrag.getBeitragStatus());
        for (Kategorie kategorie : beitrag.getKategorien()) {
            beitragDTO.getKategorieIdList().add(kategorie.getId());
        }
        model.addAttribute("kategorien", kategorieRepository.findAll());
        model.addAttribute("beitragDTO", beitragDTO);
        return "beitragBearbeiten";
    }


    @PostMapping("/beitragBearbeiten")
    public String beitragBearbeiten(@RequestParam("beitragId") long id, @ModelAttribute("beitrag") BeitragDTO beitragDTO, @ModelAttribute("backupBeitrag") BackupBeitragDTO backupBeitragDTO, BindingResult bindingResult,
                                    @ModelAttribute("currentUser") User currentUser) {
        if (bindingResult.hasErrors()) {
            return "beitragBearbeiten";
        }
        Beitrag beitrag = beitragRepository.findById(id).get();
        beitrag.setUeberschrift(beitragDTO.getUeberschrift());
        beitrag.setText(beitragDTO.getText());
        beitrag.getKategorien().clear();
        beitrag.setBeitragStatus(2);
        for (Long kategorieId : beitragDTO.getKategorieIdList()) {
            beitrag.getKategorien().add(kategorieRepository.findById(kategorieId).get());
        }
        beitrag.setAenderungsDatum(Instant.now());
        beitragRepository.save(beitrag);
        BackupBeitrag backupBeitrag = new BackupBeitrag(beitragDTO.getUeberschrift(), beitragDTO.getText(), currentUser, beitrag.getBeitragStatus());
        beitrag.getBackupBeitraege().add(backupBeitrag);
        backupBeitragRepository.save(backupBeitrag);
        return "redirect:/";
    }



    @PostMapping("/beitragLoeschen")
    public String kommentarLoeschen(@RequestParam("beitragId") Beitrag beitrag) {
        //CurrentUser ist Admin. also braucht man CurrentUser nicht, User  aus Beitrag genügt!
        BackupBeitrag backupBeitrag = new BackupBeitrag(beitrag.getUeberschrift(), beitrag.getText(), beitrag.getUser(), beitrag.getBeitragStatus());
        backupBeitrag.setLoeschDatum(Instant.now());
        backupBeitrag.setBeitragStatus(0);
        backupBeitragRepository.save(backupBeitrag);
        beitragRepository.deleteBeitragById(beitrag.getId());
        return "redirect:";
    }
}