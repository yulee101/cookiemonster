package de.awacademy.blog.Cookiemonster.beitrag;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BeitragRepository extends JpaRepository<Beitrag, Long> {
    List<Beitrag> findAllByOrderByErstellungsDatumDesc();


    @Modifying
    @Transactional
    void deleteBeitragById(long id);

}
