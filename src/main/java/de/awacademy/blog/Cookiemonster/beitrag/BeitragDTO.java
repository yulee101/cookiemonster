package de.awacademy.blog.Cookiemonster.beitrag;

import org.springframework.data.util.Pair;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BeitragDTO {

    @NotEmpty
    private String text = "";
    private String ueberschrift = "";
    private Instant aenderungsdatum;
    private List<Long> kategorieIdList = new ArrayList<>();
    private Instant loeschDatum;
    private int beitragStatus = 1;



    public BeitragDTO() {
    }

    public BeitragDTO(@NotEmpty String text, String ueberschrift, int beitragStatus) {
        this.text = text;
        this.ueberschrift = ueberschrift;
        this.aenderungsdatum = Instant.now();
        this.loeschDatum = Instant.now();
        this.beitragStatus = beitragStatus;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(Instant aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

    public List<Long> getKategorieIdList() {
        return kategorieIdList;
    }

    public void setKategorieIdList(List<Long> kategorieIdList) {
        this.kategorieIdList = kategorieIdList;
    }

    public Instant getLoeschDatum() {
        return loeschDatum;
    }

    public void setLoeschDatum(Instant loeschDatum) {
        this.loeschDatum = loeschDatum;
    }

    public int getBeitragStatus() {
        return beitragStatus;
    }

    public void setBeitragStatus(int beitragStatus) {
        this.beitragStatus = beitragStatus;
    }
}

