package de.awacademy.blog.Cookiemonster.beitrag;
import de.awacademy.blog.Cookiemonster.kategorie.Kategorie;
import de.awacademy.blog.Cookiemonster.kommentar.Kommentar;
import de.awacademy.blog.Cookiemonster.backupBeitrag.BackupBeitrag;
import de.awacademy.blog.Cookiemonster.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;
    private String ueberschrift;
    private Instant erstellungsDatum;
    private Instant aenderungsDatum;
    private Instant loeschDatum;
    private int beitragStatus = 1;

    @ManyToOne
    private User user;

    public Beitrag(){ }

    public Beitrag(String ueberschrift, String text, User user, int beitragStatus
    ) {
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.user = user;
        this.erstellungsDatum = Instant.now();
        this.aenderungsDatum = Instant.now();
        this.loeschDatum = Instant.now();
        this.beitragStatus = beitragStatus;
    }

    @ManyToMany
    private List<Kategorie> kategorien = new ArrayList<>();

    @OneToMany(mappedBy = "beitrag", cascade = CascadeType.ALL)
    @OrderBy("erstellungsDatum ASC")
    private List<Kommentar> kommentare = new ArrayList<Kommentar> ();

    @OneToMany
    @JoinColumn(name = "beitragId")
    private List<BackupBeitrag> backupBeitraege = new ArrayList<BackupBeitrag>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setErstellungsDatum(Instant erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }

    public Instant getAenderungsDatum() {
        return aenderungsDatum;
    }

    public void setAenderungsDatum(Instant aenderungsDatum) {
        this.aenderungsDatum = aenderungsDatum;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BackupBeitrag> getBackupBeitraege() {
        return backupBeitraege;
    }

    public void setBackupBeitraege(List<BackupBeitrag> backupBeitraege) {
        this.backupBeitraege = backupBeitraege;
    }

    public List<Kategorie> getKategorien() {
        return kategorien;
    }

    public Instant getLoeschDatum() {
        return loeschDatum;
    }

    public void setLoeschDatum(Instant loeschDatum) {
        this.loeschDatum = loeschDatum;
    }

    public int getBeitragStatus() {
        return beitragStatus;
    }

    public void setBeitragStatus(int beitragStatus) {
        this.beitragStatus = beitragStatus;
    }

}
