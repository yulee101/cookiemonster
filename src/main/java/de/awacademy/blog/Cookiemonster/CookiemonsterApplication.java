package de.awacademy.blog.Cookiemonster;

import de.awacademy.blog.Cookiemonster.kommentar.KommentarController;
import de.awacademy.blog.Cookiemonster.kommentar.KommentarRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookiemonsterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CookiemonsterApplication.class, args);

	}
}
