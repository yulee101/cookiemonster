package de.awacademy.blog.Cookiemonster.kontakt;

import javax.validation.constraints.NotEmpty;

public class KontaktDTO {
    @NotEmpty

    private String name ="";
    private String Text= "";
    private String email="";


    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
