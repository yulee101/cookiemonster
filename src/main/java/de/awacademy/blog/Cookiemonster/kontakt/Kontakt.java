package de.awacademy.blog.Cookiemonster.kontakt;

import de.awacademy.blog.Cookiemonster.user.User;

import javax.persistence.*;

@Entity
public class Kontakt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String email;

    @Lob
    private String Text;

    @ManyToOne
    private User user;

    private Kontakt(){
    }

    public Kontakt(String email, String name, String text, User user) {
        this.email = email;
        Text = text;
        this.user = user;
        this.name=name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
