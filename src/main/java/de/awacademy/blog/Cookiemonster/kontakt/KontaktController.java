package de.awacademy.blog.Cookiemonster.kontakt;

import de.awacademy.blog.Cookiemonster.session.SessionRepository;
import de.awacademy.blog.Cookiemonster.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class KontaktController {

    @Autowired
    KontaktRepository kontaktRepository;

    @Autowired
    SessionRepository sessionRepository;

    @GetMapping("/kontakt")
    public String kontakt(Model model){
        model.addAttribute( "kontakt", new KontaktDTO() );
        return "kontakt";
    }

    @PostMapping("/kontakt")
    public String kontakt(@ModelAttribute("kontakt") @Valid KontaktDTO kontaktDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser){
        if (bindingResult.hasErrors()) {
            return "kontakt";
        }
        Kontakt kontakt = new Kontakt( kontaktDTO.getEmail(),kontaktDTO.getName(), kontaktDTO.getText(),currentUser );
        kontaktRepository.save( kontakt );
        return "redirect:/";
    }

}
