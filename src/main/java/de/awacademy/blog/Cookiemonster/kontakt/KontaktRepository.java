package de.awacademy.blog.Cookiemonster.kontakt;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KontaktRepository extends JpaRepository<Kontakt,Long> {

}
