package de.awacademy.blog.Cookiemonster.backupBeitrag;

import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;
import de.awacademy.blog.Cookiemonster.beitrag.BeitragRepository;
import de.awacademy.blog.Cookiemonster.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BackupBeitragController {

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    BackupBeitragRepository backupBeitragRepository;

    @Autowired
    BeitragRepository beitragRepository;

    @GetMapping("/historie")
    public String backupBeitragAnzeigen(Model model) {
        model.addAttribute("backupBeitraege", backupBeitragRepository.findAllByOrderByAenderungsDatumDesc());
        return "historieAnsehen";
    }

    @PostMapping("/einzelneAenderungenAnzeigen")
    // hier Id aus html "herausgepflückt"
    public String einzelneAenderungenAnzeigen(Model model, @RequestParam ("beitragId") long id) {
        //Beitrag wird aus Repository geholt in dem ID in Repository gegeben wird
        Beitrag beitrag = beitragRepository.findById(id).get();
        //Backupelemente, die durch Beitrag geholt wurden, werden in backupBeitraege zwischengespeichert
        List<BackupBeitrag> backupBeitraege = beitrag.getBackupBeitraege();
        model.addAttribute("backupBeitraege", backupBeitraege);
        //ruft die View auf mit templatename ohne html
        return "einzelneAenderungenAnzeigen";
    }
}

