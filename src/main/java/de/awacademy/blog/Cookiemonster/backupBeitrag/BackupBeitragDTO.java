package de.awacademy.blog.Cookiemonster.backupBeitrag;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;

public class BackupBeitragDTO {

    @NotEmpty
    private String text = "";
    private String ueberschrift = "";
    private Instant aenderungsdatum;
    private Instant loeschDatum;
    private int beitragStatus = 0;


    public BackupBeitragDTO() {
    }

    public BackupBeitragDTO(@NotEmpty String text, String ueberschrift, int beitragStatus) {
        this.text = text;
        this.ueberschrift = ueberschrift;
        this.aenderungsdatum = Instant.now();
        this.loeschDatum = Instant.now();
        this.beitragStatus = beitragStatus;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(Instant aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

    public Instant getLoeschDatum() {
        return loeschDatum;
    }

    public void setLoeschDatum(Instant loeschDatum) {
        this.loeschDatum = loeschDatum;
    }

    public int getBeitragStatus() {
        return beitragStatus;
    }

    public void setBeitragStatus(int beitragStatus) {
        this.beitragStatus = beitragStatus;
    }
}
