package de.awacademy.blog.Cookiemonster.backupBeitrag;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BackupBeitragRepository extends JpaRepository<BackupBeitrag, Long> {
    List<BackupBeitrag> findAllByOrderByAenderungsDatumDesc();
}
