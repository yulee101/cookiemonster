package de.awacademy.blog.Cookiemonster.backupBeitrag;
import de.awacademy.blog.Cookiemonster.kategorie.Kategorie;
import de.awacademy.blog.Cookiemonster.kommentar.Kommentar;
import de.awacademy.blog.Cookiemonster.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BackupBeitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;
    private String ueberschrift;
    private Instant aenderungsDatum;
    private Instant loeschDatum;
    private int beitragStatus;


    @ManyToOne
    private User user;


    public BackupBeitrag(){ }

    public BackupBeitrag(String ueberschrift, String text, User user, int beitragStatus) {
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.user = user;
        this.aenderungsDatum = Instant.now();
        this.aenderungsDatum = Instant.now();
        this.loeschDatum = Instant.now();
        this.beitragStatus = beitragStatus;
    }

    @ManyToMany
    private List<Kategorie> kategorien = new ArrayList<>();

    @OneToMany(mappedBy = "beitrag", cascade = CascadeType.ALL)
    @OrderBy("erstellungsDatum ASC")
    private List<Kommentar> kommentare = new ArrayList<Kommentar> ();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getAenderungsDatum() {
        return aenderungsDatum;
    }

    public void setAenderungsDatum(Instant aenderungsDatum) {
        this.aenderungsDatum = aenderungsDatum;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getLoeschDatum() {
        return loeschDatum;
    }

    public void setLoeschDatum(Instant loeschDatum) {
        this.loeschDatum = loeschDatum;
    }

    public int getBeitragStatus() {
        return beitragStatus;
    }

    public void setBeitragStatus(int beitragStatus) {
        this.beitragStatus = beitragStatus;
    }
}
