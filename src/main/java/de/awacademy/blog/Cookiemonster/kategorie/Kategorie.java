package de.awacademy.blog.Cookiemonster.kategorie;

import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Kategorie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String bezeichnung;

    public Kategorie() {
    }

    public Kategorie(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public List<Beitrag> getBeitraege() {
        return beitraege;
    }

    public void setBeitraege(List<Beitrag> beitraege) {
        this.beitraege = beitraege;
    }

    @ManyToMany(mappedBy = "kategorien")
    List<Beitrag> beitraege = new ArrayList<>();

}
