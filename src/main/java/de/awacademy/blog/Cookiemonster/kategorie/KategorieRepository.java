package de.awacademy.blog.Cookiemonster.kategorie;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KategorieRepository extends JpaRepository<Kategorie, Long> {

    Optional<Kategorie> findFirstByBezeichnung(String bezeichnung);
    boolean existsByBezeichnung(String bezeichnung);
}
