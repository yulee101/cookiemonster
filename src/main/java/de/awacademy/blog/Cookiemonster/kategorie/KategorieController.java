package de.awacademy.blog.Cookiemonster.kategorie;


import de.awacademy.blog.Cookiemonster.beitrag.Beitrag;
import de.awacademy.blog.Cookiemonster.beitrag.BeitragRepository;
import de.awacademy.blog.Cookiemonster.kommentar.KommentarDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class KategorieController {

    @Autowired
    KategorieRepository kategorieRepository;

    @Autowired
    private BeitragRepository beitragRepository;



    @GetMapping("/kategorieAnzeigen")
    public String kategorieAnzeigen(Model model, @RequestParam ("kategorieId") long id) {
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByErstellungsDatumDesc());
        model.addAttribute("kommentar", new KommentarDTO());
        model.addAttribute("benutzerverwaltung");
        model.addAttribute("kategorien", kategorieRepository.findAll());
        List<Beitrag> beitraege = new ArrayList<>();

        for (Beitrag beitrag : beitragRepository.findAllByOrderByErstellungsDatumDesc()) {

            List<Kategorie> beitragsKategorien = new ArrayList<>();
            beitragsKategorien.addAll(beitrag.getKategorien());

            if (beitragsKategorien.contains(kategorieRepository.findById(id).get())) {
                beitraege.add(beitrag);
            }
        }
        model.addAttribute("beitraegeDerKategorie", beitraege);

        return "kategorieAnzeigen";
    }

    @GetMapping("kategorieErstellen")
    public String kategorieErstellen(Model model) {
        model.addAttribute("kategorie", new KategorieDTO());
        return "kategorieErstellen";
    }

    @PostMapping("/kategorieErstellen")
    public String kategorieErstellen(@ModelAttribute ("kategorie") @Valid KategorieDTO kategorieDTO, BindingResult bindingResult) {
//        if (kategorieRepository.existsByBezeichnung(kategorieDTO.getBezeichnung())) {
//            bindingResult.addError(new FieldError("kategorie", "bezeichnung", "Bezeichnung existiert bereits."));
//        }
        if (bindingResult.hasErrors()) {
            return "kategorieErstellen";
        }

        Kategorie kategorie = new Kategorie(kategorieDTO.getBezeichnung());
        kategorieRepository.save(kategorie);
        return "redirect:/";
    }

}
