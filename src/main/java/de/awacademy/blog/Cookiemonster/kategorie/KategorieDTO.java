package de.awacademy.blog.Cookiemonster.kategorie;

public class KategorieDTO {

    private String bezeichnung = "";

    public KategorieDTO(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public KategorieDTO() {
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
