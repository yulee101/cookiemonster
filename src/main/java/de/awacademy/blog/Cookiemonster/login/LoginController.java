package de.awacademy.blog.Cookiemonster.login;

import de.awacademy.blog.Cookiemonster.session.SessionRepository;
import de.awacademy.blog.Cookiemonster.user.User;
import de.awacademy.blog.Cookiemonster.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class LoginController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    private LoginService loginService;


    @GetMapping("login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("login")
    public String loginSubmit(Model model, LoginDTO login, HttpServletResponse response) {
        Optional<User> optionalUser = userRepository.findFirstByUsernameAndPassword(login.getUsername(), login.getPassword());
        if (optionalUser.isPresent()) {
            loginService.login(optionalUser.get(), response);
            return "redirect:/";
        }
        model.addAttribute("login", login);

        return "login";
    }

    @PostMapping("logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        sessionRepository.deleteById(sessionId);
        Cookie c = new Cookie("sessionId", "");
        c.setMaxAge(0);
        response.addCookie(c);
        return "redirect:/";
    }

    @PostMapping("loginAdmin")
    public String loginAdmin(Model model, LoginDTO login, HttpServletResponse response, User user) {
        Optional<User> optionalUser = userRepository.findFirstByUsernameAndPassword(login.getUsername(), login.getPassword());
        if (optionalUser.isPresent() && user.isAdmin()) {
            loginService.login(optionalUser.get(), response);
            return "users";
        }
        model.addAttribute("loginAdmin", login);

        return "loginAdmin";
    }
}