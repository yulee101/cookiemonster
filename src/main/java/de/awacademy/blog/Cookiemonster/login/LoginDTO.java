package de.awacademy.blog.Cookiemonster.login;

import de.awacademy.blog.Cookiemonster.user.User;

public class LoginDTO {
    private String username = "";
    private String password = "";

    User user;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
